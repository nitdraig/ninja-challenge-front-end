# Front-end desarrollado para el Ninja Challenge

1- Usando create-react-app, empiezo el proyecto.
2- Usé axios para llamar la API
3- En un componente se Listan los usuarios usando un mapeo, renderizando en a tráves de un componente Card que al clickearlo en "Ver más" se puede Revisar más información sobre cada usuario en un componente de Detalles.

URL DEPLOY: https://ninja-challenge-fe-ten.vercel.app
URL CÓDIGO: https://gitlab.com/nitdraig/ninja-challenge-front-end.git - https://github.com/nitdraig/ninja-challenge-fe
